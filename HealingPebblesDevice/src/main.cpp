#include <Arduino.h>
#include "BluetoothSerial.h"

BluetoothSerial ESP_BT; // Object for Bluetooth

uint16_t MeasurementNumber = 0;
const uint16_t Freq = 500;
const uint16_t DiscriptSize = 21;
std::vector<uint16_t> data;
std::vector<uint8_t> buffer;

// Состояние подключения Bluetooth
boolean BT_cnx = false;

void callback(esp_spp_cb_event_t event, esp_spp_cb_param_t *param){
  if(event == ESP_SPP_SRV_OPEN_EVT){
    Serial.println("Client Connected");
    digitalWrite(LED_BUILTIN, HIGH);
    BT_cnx = true;
  }
 
  if(event == ESP_SPP_CLOSE_EVT ){
    Serial.println("Client disconnected");
    digitalWrite(LED_BUILTIN, LOW);
    BT_cnx = false;
    ESP.restart();
  }
}

void SetPackageTitle(std::vector<uint8_t>* buffer){
  std::vector<uint8_t> title = {69, 67, 71, 32, 68, 65, 84, 65, 34};
  (*buffer).insert((*buffer).end(), title.begin(), title.end());
}
void SetPackageHeader(std::vector<uint8_t>* buffer, uint16_t packageLength, uint16_t samplingFrequency, uint8_t numofBit, uint16_t umv, uint8_t numOfChenal, uint16_t measurementNumber){
  (*buffer).push_back(packageLength % 256);
  (*buffer).push_back(packageLength / 256);
  (*buffer).push_back(samplingFrequency % 256);
  (*buffer).push_back(samplingFrequency / 256);
  (*buffer).push_back(numofBit);
  (*buffer).push_back(umv % 256);
  (*buffer).push_back(umv / 256);
  (*buffer).push_back(numOfChenal);
  (*buffer).push_back(measurementNumber % 256);
  (*buffer).push_back(measurementNumber / 256);
}

void SetData(std::vector<uint8_t>* buffer, const std::vector<uint16_t>& data){
  (*buffer).insert((*buffer).end(), data.begin(), data.end());
  (*buffer).push_back(34);
  (*buffer).push_back(13);
  (*buffer).push_back(10);
}

void setup() {
  Serial.begin(115200);
  pinMode(41, INPUT); // Setup for leads off detection LO +
  pinMode(40, INPUT); // Setup for leads off detection LO -
  MeasurementNumber = 0;
  ESP_BT.register_callback(callback);
  if(!ESP_BT.begin("HealingPebbles")){
    Serial.println("An error occurred initializing Bluetooth");
  }else{
    Serial.println("Bluetooth initialized... Bluetooth Device is Ready to Pair...");
  }
}

void loop() {
  if((digitalRead(40) == 1)||(digitalRead(41) == 1)){
    Serial.println('!');
    buffer.clear();
    data.clear();
  }
  else{
    if (BT_cnx){
      if (data.size() == Freq) {
        buffer.clear();
        SetPackageTitle(&buffer);
        SetPackageHeader(&buffer, Freq + DiscriptSize, Freq, 8, 2288, 1, MeasurementNumber);
        SetData(&buffer, data);
        ESP_BT.write(buffer.data(), buffer.size());
        //Serial.write(buffer.data(), buffer.size());
        Serial.println();
        data.clear();
        MeasurementNumber += 1;
      }
      else {
        data.push_back(analogRead(A0));
      }
    }
  }
  delay(2);
}